﻿namespace xxhash_fsharp

module XXH32 =
    let hash (x : uint8[]) (seed : uint32) : uint32 =
        let rotl (x : uint32) (i : int) : uint32 =
            (x <<< (i % 32)) ||| (x >>> (32 - (i % 32)))
        let read_le (x : uint8[]) (i : int) : uint32 =
            (uint32 x.[i])
            ||| ((uint32 x.[i+1]) <<< 8)
            ||| ((uint32 x.[i+2]) <<< 16)
            ||| ((uint32 x.[i+3]) <<< 24)
        let xlen = Array.length x
        let PRIME32_1 : uint32 = 0x9e3779b1u
        let PRIME32_2 : uint32 = 0x85ebca77u
        let PRIME32_3 : uint32 = 0xc2b2ae3du
        let PRIME32_4 : uint32 = 0x27d4eb2fu
        let PRIME32_5 : uint32 = 0x165667b1u
        let avalanche (x : uint32) : uint32 =
            let acc = x ^^^ (x >>> 15)
            let acc = acc * PRIME32_2
            let acc = acc ^^^ (acc >>> 13)
            let acc = acc * PRIME32_3
            let acc = acc ^^^ (acc >>> 16)
            in acc
        let round (acc : uint32) (lane : uint32) : uint32 =
            let acc = acc + (lane * PRIME32_2)
            let acc = rotl acc 13
            let acc = acc * PRIME32_1
            acc
        let mutable i = 0
        let acc : uint32 =
            let mutable preres : uint32 = 
                if xlen < 16
                then seed + PRIME32_5
                else
                    let mutable acc1 : uint32 = seed + PRIME32_1 + PRIME32_2
                    let mutable acc2 : uint32 = seed + PRIME32_2
                    let mutable acc3 : uint32 = seed + 0u
                    let mutable acc4 : uint32 = seed - PRIME32_1
                    while xlen - i >= 16 do
                        acc1 <- round acc1 (read_le x i)
                        i <- i + 4
                        acc2 <- round acc2 (read_le x i)
                        i <- i + 4
                        acc3 <- round acc3 (read_le x i)
                        i <- i + 4
                        acc4 <- round acc4 (read_le x i)
                        i <- i + 4
                    (rotl acc1 1) + (rotl acc2 7) + (rotl acc3 12) + (rotl acc4 18)
            preres <- preres + uint32 xlen
            while xlen - i >= 4 do
                preres <- preres + (read_le x i) * PRIME32_3
                preres <- (rotl preres 17) * PRIME32_4
                i <- i + 4
            while i < xlen do
                preres <- preres + (uint32 x.[i]) * PRIME32_5
                preres <- (rotl preres 11) * PRIME32_1
                i <- i + 1
            preres
        in avalanche acc
        
module XXH64 =
    let hash (x : uint8[]) (seed : uint64) : uint64 =
        let rotl (x : uint64) (i : int) : uint64 =
            (x <<< (i % 64)) ||| (x >>> (64 - (i % 64)))
        let read_le (x : uint8[]) (i : int) : uint64 =
            (uint64 x.[i])
            ||| ((uint64 x.[i+1]) <<< 8)
            ||| ((uint64 x.[i+2]) <<< 16)
            ||| ((uint64 x.[i+3]) <<< 24)
            ||| ((uint64 x.[i+4]) <<< 32)
            ||| ((uint64 x.[i+5]) <<< 40)
            ||| ((uint64 x.[i+6]) <<< 48)
            ||| ((uint64 x.[i+7]) <<< 56)
        let read32_le (x : uint8[]) (i : int) : uint64 =
            (uint64 x.[i])
            ||| ((uint64 x.[i+1]) <<< 8)
            ||| ((uint64 x.[i+2]) <<< 16)
            ||| ((uint64 x.[i+3]) <<< 24)
        let xlen = Array.length x
        let PRIME64_1 : uint64 = uint64 0x9e3779b185ebca87UL
        let PRIME64_2 : uint64 = uint64 0xc2b2ae3d27d4eb4fUL
        let PRIME64_3 : uint64 = uint64 0x165667b19e3779f9UL
        let PRIME64_4 : uint64 = uint64 0x85ebca77c2b2ae63UL
        let PRIME64_5 : uint64 = uint64 0x27d4eb2f165667c5UL
        let round (accN : uint64) (laneN : uint64) : uint64 =
            let mutable preres : uint64 = accN
            preres <- preres + (laneN * PRIME64_2)
            preres <- rotl preres 31
            preres * PRIME64_1
        let merge_accumulator (acc : uint64) (accN : uint64) : uint64 =
            let mutable res : uint64 = acc
            res <- res ^^^ (round 0UL accN)
            res <- res * PRIME64_1
            res + PRIME64_4
        let avalanche (x : uint64) : uint64 =
            let acc = x ^^^ (x >>> 33)
            let acc = acc * PRIME64_2
            let acc = acc ^^^ (acc >>> 29)
            let acc = acc * PRIME64_3
            let acc = acc ^^^ (acc >>> 32)
            in acc
        let mutable i = 0
        let acc : uint64 =
            let mutable preres : uint64 =
                if xlen < 32
                then seed + PRIME64_5
                else
                    let mutable acc1 : uint64 = seed + PRIME64_1 + PRIME64_2
                    let mutable acc2 : uint64 = seed + PRIME64_2
                    let mutable acc3 : uint64 = seed + 0UL
                    let mutable acc4 : uint64 = seed - PRIME64_1
                    while xlen - i >= 32 do
                        acc1 <- round acc1 (read_le x i)
                        i <- i + 8
                        acc2 <- round acc2 (read_le x i)
                        i <- i + 8
                        acc3 <- round acc3 (read_le x i)
                        i <- i + 8
                        acc4 <- round acc4 (read_le x i)
                        i <- i + 8
                    let mutable z : uint64 = 0UL
                    z <- (rotl acc1 1) + (rotl acc2 7) + (rotl acc3 12) + (rotl acc4 18)
                    z <- merge_accumulator z acc1
                    z <- merge_accumulator z acc2
                    z <- merge_accumulator z acc3
                    z <- merge_accumulator z acc4
                    z
            preres <- preres + uint64 xlen
            while xlen - i >= 8 do
                let lane = read_le x i
                preres <- preres ^^^ (round 0UL lane)
                preres <- (rotl preres 27) * PRIME64_1
                preres <- preres + PRIME64_4
                i <- i + 8
            while xlen - i >= 4 do
                let lane : uint64 = read32_le x i
                preres <- preres ^^^ (lane * PRIME64_1)
                preres <- (rotl preres 23) * PRIME64_2
                preres <- preres + PRIME64_3
                i <- i + 4
            while i < xlen do
                let lane = x.[i]
                preres <- preres ^^^ (uint64 lane) * PRIME64_5
                preres <- (rotl preres 11) * PRIME64_1
                i <- i + 1
            preres
        in avalanche acc
        