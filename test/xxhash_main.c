#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#define XXH_STATIC_LINKING_ONLY   /* access advanced declarations */
#define XXH_IMPLEMENTATION   /* access definitions */
#include "xxhash.h"

uint8_t buf[1024];
size_t len;

void gen() {
    size_t randlen = (size_t)floor(((double)rand())/RAND_MAX*1024);
    // size_t randlen = (size_t)floor(((double)rand())/RAND_MAX*16);
    // size_t randlen = (size_t)floor(((double)rand())/RAND_MAX*32);
    size_t i;
    for (i = 0; i < randlen; i++) {
        buf[i] = (uint8_t)floor(((double)rand())/RAND_MAX*256);
    }
    len = randlen;
}

/* Generate output format:
       seed (in hex)
       (then a list of test cases, each test case takes up a single line
        that ends with \n, with the following format)
            length (in decimal)
            \t
            random data of the length (in hex)
            \t
            xxh32 (in hex)
            \t
            xxh64 (in hex)
*/

void writedata(FILE* x) {
    size_t i;
    for (i = 0; i < len; i++) {
        fprintf(x, "%02x", buf[i]);
    }
    fflush(x);
}

int main() {
    int i = 0;
    FILE* output;
    // uint32_t seed = 0;
    uint32_t seed = time(NULL);
    output = fopen("xxhash_test.txt", "w");
    fprintf(output, "%08x\n", seed);
    fflush(output);

    srand(seed);


    for (i = 0; i < 512; i++) {
        gen();
        fprintf(output, "%d\t", len);
        writedata(output);
        fputc('\t', output);
        XXH32_hash_t hash32 = XXH32(buf, len, seed);
        fprintf(output, "%016x\t", hash32);
        XXH64_hash_t hash64 = XXH64(buf, len, (uint64_t)seed);
        fprintf(output, "%016llx\t", hash64);
        fputc('\n', output);
        fflush(output);
    }


    fclose(output);
    return 0;
}
