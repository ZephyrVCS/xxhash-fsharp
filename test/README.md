# How to test

Basically this program generates a set of 512 random test cases.

1.  Compile `xxhash_main.c`. You probably need to have `xxhash.h` at the same folder.
2.  Run the compiled executable to obtain `xxhash_test.txt`
3.  Compile `xxhash_test.fs` with `fsc ../Library.fs XxhashTest.fs`.
4.  Run the compiled executable to test.
5.  Check if the output contains `XXH32: FAIL` or `XXH64: FAIL`. If it does, then the implementation is wrong.
