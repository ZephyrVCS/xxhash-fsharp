

let parseCh (x : char) : uint8 =
    System.Convert.ToByte x - (
        if '0' <= x && x <= '9' then 48uy
        else if 'a' <= x && x <= 'f' then 87uy
        else 55uy
    )

let parseUInt32 (x : string) : uint32 =
    let mutable res : uint32 = 0u
    let mutable i : int = 0
    let len = String.length x
    while i < len do
        res <- (res * 16u) + uint32 (parseCh x.[i])
        i <- i + 1
    res

let parseUInt64 (x : string) : uint64 =
    let mutable res : uint64 = 0UL
    let mutable i : int = 0
    let len = String.length x
    while i < len do
        res <- (res * 16UL) + uint64 (parseCh x.[i])
        i <- i + 1
    res

let parseUInt8Array (x : string) (buf : uint8[]) : unit =
    let mutable i : int = 0
    let mutable j : int = 0
    let len = String.length x
    while i < len do
        buf.[j] <- ((parseCh x.[i]) <<< 4) ||| (parseCh x.[i+1])
        j <- j + 1
        i <- i + 2

let readCase (x : string) : int * uint8[] * uint32 * uint64 =
    let ps = x.Split('\t')
    let clen = System.Convert.ToInt32 ps.[0]
    let buf : uint8[] = Array.zeroCreate clen
    parseUInt8Array ps.[1] buf
    (System.Convert.ToInt32 ps.[0], buf, parseUInt32 ps.[2], parseUInt64 ps.[3])

[<EntryPoint>]
let main (args : string[]) =
    let source : string array = System.IO.File.ReadAllLines("xxhash_test.txt")
    let seed32 : uint32 = parseUInt32 source.[0]
    let mutable i : int = 0
    printfn "Seed: %x" seed32
    while i < 512 do
        let (clen, cbuf, chash32, chash64) = readCase source.[i+1]
        let h32 = xxhash_fsharp.XXH32.hash cbuf seed32
        let h64 = xxhash_fsharp.XXH64.hash cbuf (uint64 seed32)
        printfn "Case %d Len: %d XXH32: %s, %x vs %x" i clen (if chash32 = h32 then "OK" else "FAIL") chash32 h32
        printfn "Case %d Len: %d XXH64: %s, %x vs %x" i clen (if chash64 = h64 then "OK" else "FAIL") chash64 h64
        i <- i + 1
    0
